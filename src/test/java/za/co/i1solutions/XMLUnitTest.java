package za.co.i1solutions;

import org.junit.Test;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.Difference;
import za.co.i1solutions.domain.XmlDiff;
import za.co.i1solutions.util.XMLDiffUtility;

import java.util.Iterator;

import static org.junit.Assert.assertThat;


public class XMLUnitTest {


    @Test
    public void given2XMLS_whenGeneratesDifferences_thenCorrect() {
        String controlXml = "<struct><boolean>false         </boolean><int>3</int></struct>";
        String testXml = "<struct><boolean>false</boolean><int>" +
                "3 " +
                "</int></struct>";
        Diff myDiff = DiffBuilder.compare(controlXml)
                .withTest(testXml)
                .ignoreWhitespace()
                .checkForSimilar()
                .build();

        Iterator<Difference> iter = myDiff.getDifferences().iterator();
        int size = 0;
        while (iter.hasNext()) {
            System.out.println(iter.next().toString());
            size++;
        }
        //assertThat(size,);
    }

}