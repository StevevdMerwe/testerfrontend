package za.co.i1solutions;

import za.co.i1solutions.db.DBConnection;
import za.co.i1solutions.routes.ApiRoutes;
import za.co.i1solutions.routes.TemplateEngineRoutes;

public class RestApi {

    public static void main(String[] args) {
        // initialize the database connections
        DBConnection.initialize();
        // register routes
        new TemplateEngineRoutes().routes();
        new ApiRoutes().routes();
    }
}
