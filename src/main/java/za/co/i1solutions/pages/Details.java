package za.co.i1solutions.pages;

import org.aeonbits.owner.ConfigFactory;
import spark.Request;
import za.co.i1solutions.db.DBConnection;
import za.co.i1solutions.domain.Summary;
import za.co.i1solutions.util.ServerConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static za.co.i1solutions.util.XMLDiffUtility.calculateDiff;

public class Details extends Page {
    static ServerConfig cfg = ConfigFactory.create(ServerConfig.class);
    private List<Map<String, Object>> detail;
    private String uuid;
    private String QUERY = cfg.projectSummary();

    public Details(Request req) {
        uuid = req.params(":uuid");
    }

    public Map<String, Object> getProject() {
        Map<String, Object> map = new HashMap<>();
        map.put("detail", getProjectSummary(uuid));
        return map;
    }

    private List<Summary> getProjectSummary(String uuid) {
        List<Summary> sum = DBConnection.jdbi().withHandle(handle ->
                handle.createQuery(QUERY)
                        .bind("UUID", uuid)
                        .mapToBean(Summary.class)
                        .list());

        calculateDiff(sum, false);
        return sum;
    }
}
