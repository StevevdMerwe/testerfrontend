package za.co.i1solutions.pages;


import org.aeonbits.owner.ConfigFactory;
import spark.Request;
import za.co.i1solutions.db.DBConnection;
import za.co.i1solutions.domain.Summary;
import za.co.i1solutions.util.ServerConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static za.co.i1solutions.util.XMLDiffUtility.calculateDiff;

public class ProjectDetails extends Page {
    private static ServerConfig cfg = ConfigFactory.create(ServerConfig.class);
    private String QUERY = cfg.projectQueryWithProjectName();
    private String project;

    public ProjectDetails(Request req) {
        project = req.params(":project");
    }

    public List<Summary> getDataList() {
        return getProjectSummary(project);
    }

    public Map<String, Object> getProject() {
        Map<String, Object> map = new HashMap<>();
        map.put("project", project);
        return map;
    }

    private List<Summary> getProjectSummary(String project) {
        List<Summary> sum = DBConnection.jdbi().withHandle(handle ->
                handle.createQuery(QUERY)
                        .bind("project", project)
                        .mapToBean(Summary.class)
                        .list());

        calculateDiff(sum, true);
        return sum;
    }
}
