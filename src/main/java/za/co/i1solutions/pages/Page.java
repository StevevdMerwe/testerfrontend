package za.co.i1solutions.pages;

import java.util.HashMap;
import java.util.Map;

abstract class Page {

    protected Map map = new HashMap();

    public Map<String, Object> getData() {
        return map;
    }

}
