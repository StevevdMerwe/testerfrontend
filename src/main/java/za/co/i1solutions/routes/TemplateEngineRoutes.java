package za.co.i1solutions.routes;

import org.aeonbits.owner.ConfigFactory;
import spark.ModelAndView;
import spark.template.thymeleaf.ThymeleafTemplateEngine;
import za.co.i1solutions.pages.Details;
import za.co.i1solutions.pages.ProjectDetails;
import za.co.i1solutions.util.ServerConfig;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;
import static spark.Spark.staticFiles;

public class TemplateEngineRoutes extends RouteRegistrar {
    static ServerConfig cfg = ConfigFactory.create(ServerConfig.class);

    public void routes() {

        setup();

        get("/", (req, res) -> render("index"));
        get("/index", (req, res) -> render("index"));
        get("/calendar", (req, res) -> render("calendar"));
        get("/help", (req, res) -> render("help"));
        get("/tests", (req, res) -> render(new ProjectDetails(req).getProject(), ("tests")));
        get("/tests/:project", (req, res) -> render(new ProjectDetails(req).getProject(), ("tests")));
        get("/details/:uuid", (req, res) -> render(new Details(req).getProject(), "details"));
        get("/config", (req, res) -> render("config"));

    }

    private void setup() {
        if (cfg.externalLocation()) {
            staticFiles.externalLocation(cfg.staticFilesLocation());
        } else {
            // root is 'src/main/resources', so put files in 'src/main/resources/public'
            staticFiles.location("/public");
        }
    }

    public String render(Map<String, Object> model, String templatePath) {
        return new ThymeleafTemplateEngine().render(new ModelAndView(model, templatePath));
    }

    public String render(String templatePath) {
        return new ThymeleafTemplateEngine().render(new ModelAndView(new HashMap(), templatePath));
    }

}
