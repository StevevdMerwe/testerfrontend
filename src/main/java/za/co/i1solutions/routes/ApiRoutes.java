package za.co.i1solutions.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import za.co.i1solutions.db.DatabaseCalls;
import za.co.i1solutions.domain.ConfigData;
import za.co.i1solutions.domain.Projects;
import za.co.i1solutions.domain.Summary;
import za.co.i1solutions.pages.ProjectDetails;
import za.co.i1solutions.util.JsonTransformer;

import java.util.List;

import static spark.Spark.*;

public class ApiRoutes extends RouteRegistrar {
    private static final Logger logger = LoggerFactory.getLogger(ApiRoutes.class);

    DatabaseCalls db = new DatabaseCalls();

    // Enables CORS on requests. This method is an initialization method and should be called once.
    private static void enableCORS(final String route, final String origin, final String methods, final String headers) {

        options(route, (request, response) -> {
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }
            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
            return "OK";
        });

        before(route, (request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
            response.header("Access-Control-Request-Method", methods);
            response.header("Access-Control-Allow-Headers", headers);
            response.type("application/json");
        });
    }

    public void routes() {
        enableCORS("/api/*", "*", "*", "*");
        logging();

        path("/api/", () -> {
            get("/tests/", (Request request, Response response) -> {
                List<Summary> latest = db.getSummary();
                if (latest == null) response.status(404);
                return latest;
            }, new JsonTransformer());

            get("/tests/:project", (Request request, Response response) -> {
                List<Summary> latest = new ProjectDetails(request).getDataList();
                if (latest == null) response.status(404);
                return latest;
            }, new JsonTransformer());

            get("/projects/", (Request request, Response response) -> {
                List<Projects> latest = db.getProjects();
                if (latest == null) response.status(404);
                return latest;
            }, new JsonTransformer());

            get("/config/", (Request request, Response response) -> {
                List<ConfigData> latest = db.getConfig();
                if (latest == null) response.status(404);
                return latest;
            }, new JsonTransformer());

        });
    }

    private void logging() {
        before("/*", (q, a) -> logger.info("Received api call " + q.uri()));
        after("/*", (q, a) -> logger.debug("Responded api call " + q.uri() + " with :" + a.status() + " " + a.body()));
    }
}