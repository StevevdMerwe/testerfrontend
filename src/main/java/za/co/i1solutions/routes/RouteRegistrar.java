package za.co.i1solutions.routes;

public abstract class RouteRegistrar {
    abstract void routes();
}
