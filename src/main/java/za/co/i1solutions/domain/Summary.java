package za.co.i1solutions.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Summary {
    String sid;
    String testRunDatetime;
    String projectName;
    String testRunUuid;
    String testUuid;
    String sequenceNumber;
    String httpEndpoint;
    String mqManagerName;
    String mqQueueName;
    String additionalParameters;
    String testDatetime;
    String requestData;
    String expectedResponseData;
    String responseData;
    boolean problem;
    String difference;
}
