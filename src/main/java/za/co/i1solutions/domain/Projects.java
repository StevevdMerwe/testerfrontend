package za.co.i1solutions.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Projects {
    String testRunDatetime;
    String testRunUuid;
    String testUuid;
}
