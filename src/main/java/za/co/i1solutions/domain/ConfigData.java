package za.co.i1solutions.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigData {
    String sid;
    String projectName;
    String projectType;
    String httpEndpoint;
    String mqManagerName;
    String mqQueueName;
}
