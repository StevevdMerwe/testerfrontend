package za.co.i1solutions.db;

import org.aeonbits.owner.ConfigFactory;
import za.co.i1solutions.domain.ConfigData;
import za.co.i1solutions.domain.Projects;
import za.co.i1solutions.domain.Summary;
import za.co.i1solutions.util.ServerConfig;

import java.util.List;

import static za.co.i1solutions.util.XMLDiffUtility.calculateDiff;

public class DatabaseCalls {
    static ServerConfig cfg = ConfigFactory.create(ServerConfig.class);

    public static List<Summary> getSummaries(String Query) {
        List<Summary> sum = DBConnection.jdbi().withHandle(handle ->
                handle.createQuery(Query).mapToBean(Summary.class)
                        .list());
        calculateDiff(sum, true);
        return sum;
    }

    public List<Summary> getSummary() {
        return getSummaries(cfg.testLogQuery());
    }


    public List<ConfigData> getConfig() {
        return DBConnection.jdbi().withHandle(handle ->
                handle.createQuery(cfg.configQuery())
                        .mapToBean(ConfigData.class)
                        .list());
    }

    public List<Projects> getProjects() {
        return DBConnection.jdbi().withHandle(handle ->
                handle.createQuery(cfg.projectQuery())
                        .mapToBean(Projects.class)
                        .list());
    }
}
