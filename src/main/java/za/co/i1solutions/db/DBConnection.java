package za.co.i1solutions.db;

import org.aeonbits.owner.ConfigFactory;
import org.jdbi.v3.core.Jdbi;
import za.co.i1solutions.util.ServerConfig;


public final class DBConnection {

    static ServerConfig cfg = ConfigFactory.create(ServerConfig.class);
    private static DBConnection thisObject;

    private Jdbi jdbi;

    private DBConnection() {
        System.out.println("===========> " + cfg.welcome());
        jdbi = Jdbi.create(cfg.SqlHost(), cfg.SqlUser(), cfg.SqlPassword());
        thisObject = this;
    }

    public static void initialize() {
        new DBConnection();
    }

    public static Jdbi jdbi() {
        return thisObject.jdbi;
    }
}
