package za.co.i1solutions.util;

import org.aeonbits.owner.Config;

@Config.Sources({"file:~/ServerConfig.properties",
        "classpath:ServerConfig.properties"})
public interface ServerConfig extends Config {

    @DefaultValue("Loading from Class!!!!!!!!!!!!!!")
    String welcome();

    @DefaultValue("jdbc:postgresql://127.0.0.1:54322/javacode")
    String SqlHost();

    @DefaultValue("postgres")
    String SqlUser();

    @DefaultValue("password")
    String SqlPassword();

    @DefaultValue("false")
    boolean externalLocation();

    @DefaultValue("/public")
    String staticFilesLocation();

    @DefaultValue("SELECT \"SID\", \"TESTRUNDATETIME\", \"PROJECTNAME\" ,\"TESTRUNUUID\", \"TESTUUID\", \"SEQUENCENUMBER\", " +
            "\"HTTPENDPOINT\", \"MQMANAGERNAME\", \"MQQUEUENAME\", \"ADDITIONALPARAMETERS\", " +
            "\"TESTDATETIME\", \"REQUESTDATA\", \"EXPECTEDRESPONSEDATA\", \"RESPONSEDATA\" " +
            "FROM \"TESTLOG\" order by \"TESTRUNUUID\",\"SEQUENCENUMBER\" ")
    String testLogQuery();

    @DefaultValue("SELECT \"SID\", \"PROJECTNAME\", \"PROJECTTYPE\"," +
            " \"HTTPENDPOINT\", \"MQMANAGERNAME\", \"MQQUEUENAME\" " +
            "FROM \"CONFIG\"")
    String configQuery();

    @DefaultValue("SELECT   distinct(\"TESTRUNUUID\"),\"PROJECTNAME\",\"TESTRUNDATETIME\"  \n" +
            "FROM \"TESTLOG\" " +
            "order by \"PROJECTNAME\" desc")
    String projectQuery();

    @DefaultValue("SELECT \"SID\", \"TESTRUNDATETIME\", \"PROJECTNAME\" ,\"TESTRUNUUID\", \"TESTUUID\", \"SEQUENCENUMBER\", " +
            "\"HTTPENDPOINT\", \"MQMANAGERNAME\", \"MQQUEUENAME\", \"ADDITIONALPARAMETERS\", " +
            "\"TESTDATETIME\", \"REQUESTDATA\", \"EXPECTEDRESPONSEDATA\", \"RESPONSEDATA\" " +
            "FROM \"TESTLOG\" " +
            "WHERE \"TESTUUID\" = :UUID ")
    String projectSummary();

    @DefaultValue("SELECT \"SID\", \"TESTRUNDATETIME\", \"PROJECTNAME\" ,\"TESTRUNUUID\", \"TESTUUID\", \"SEQUENCENUMBER\", " +
            "\"HTTPENDPOINT\", \"MQMANAGERNAME\", \"MQQUEUENAME\", \"ADDITIONALPARAMETERS\", " +
            "\"TESTDATETIME\", \"REQUESTDATA\", \"EXPECTEDRESPONSEDATA\", \"RESPONSEDATA\" " +
            "FROM \"TESTLOG\" " +
            "where \"PROJECTNAME\" = :project ")
    String projectQueryWithProjectName();


}

