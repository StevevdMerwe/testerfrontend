package za.co.i1solutions.util;

import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Diff;
import za.co.i1solutions.domain.Summary;
import za.co.i1solutions.domain.XmlDiff;

import java.util.List;

public class XMLDiffUtility {
    public static XmlDiff compareXML(String expected, String response) {
        String error;
        try {
            Diff myDiff = DiffBuilder.compare(Input.fromString(expected))
                    .withTest(Input.fromString(response))
                    .build();

            return new XmlDiff(myDiff.toString(), myDiff.hasDifferences());
        } catch (org.xmlunit.XMLUnitException xmle) {
            error = xmle.getCause().getMessage();
        }
        return new XmlDiff(error, true);
    }

    public static void calculateDiff(List<Summary> sum, boolean hideBigData) {
        for (Summary item : sum) {
            String exp = item.getExpectedResponseData();
            String responseData = item.getResponseData();
            if (exp != null && responseData != null) {
//                exp = exp.replaceAll("</Status>",
//                        "Steve</Status>");
                XmlDiff diff = XMLDiffUtility.compareXML(exp, responseData);
                item.setProblem(!diff.isDifferent());
                item.setDifference(diff.getXml());
            } else {
                item.setProblem(false);
                String message = "One element is null";
                if (exp == null) {
                    message = "Expected response is empty";
                } else if (responseData == null) {
                    message = "Actual Response is empty";
                }
                item.setDifference(message);
            }
            if (hideBigData) {
                item.setResponseData(null);
                item.setRequestData(null);
                item.setExpectedResponseData(null);
            }
        }
    }
}
