i1mon = {

    initChartJS: function (tag, type, metric, label, color) {
        var config = {
            type: 'line',
            options: {
                responsive: true,
                title: {
                    display: false,
                    //text: label
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'series',
                        ticks: {
                            source: 'labels'
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: label
                        }
                    }]
                }
            }
        };

        function callAjax(tag, config, metric, type) {
            jQuery.ajax({
                //monitoring/metrics?metric=MaximumCPUTime&type=node&timePeriod=NINETY_DAYS&aggregationInterval=day
                url: "/monitoring/metrics?metric=" + metric + "&type=" + type + '&timePeriod=NINETY_DAYS&aggregationInterval=day',
                type: "GET",
                success: function (data) {
                    var ctx = document.getElementById(tag).getContext("2d");
                    var timechart = new Chart(ctx, config);
                    addData(timechart, data.series[0].data, data.series[0].name)
                },
                error: function (request, status, error) {
                    // alert("Problem getting data from server " + request.responseText + " " + error);
                    console.log("Problem getting data from server " + request.responseText + " " + error)
                }
            });
        }

        function addData(chart, data, label) {
            var newDataset = {
                label: label,
                data: data,
                backgroundColor: color,
                borderColor: color,
            };
            chart.data.datasets.push(newDataset);
            chart.update();
        }

        callAjax(tag, config, metric, type);

    },


    initDropDown: function (type, tag) {
        function getDropdown(type, tag) {
            jQuery.ajax({
                url: "/monitoring/tagsForType?type=" + type,
                type: "GET",
                success: function (data) {
                    callback(data, tag);
                },
                error: function (request, status, error) {
                    console.log("Problem getting data from server " + request.responseText + " " + error)
                }
            });
        }

        function callback(data, tag) {
            console.log(data);
            // Get select
            var select = document.getElementById(tag);
            select.options.length = 0;
            // // Add options
            var count = 0;
            for (var i in data) {
                select.options[select.options.length] = new Option(data[i], data[i]);
                if (count = 0) $('.selectpicker').selectpicker('val', data[i]);
            }

            $('.selectpicker').selectpicker('refresh');

        }

        getDropdown(type, tag)
    },

    updateChart: function (tag, type, metric, label, color,search,period,aggregation) {
        var config = {
            type: 'line',
            options: {
                responsive: true,
                title: {
                    display: false,
                    //text: label
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            displayFormats: {
                                quarter: 'MMM YYYY'
                            }
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: label
                        }
                    }]
                }
            }
        };




        function callAjax(tag, config, metric, type) {
            jQuery.ajax({
                //monitoring/metrics?metric=MaximumCPUTime&type=node&timePeriod=NINETY_DAYS&aggregationInterval=day
                url: "/monitoring/metrics?metric=" + metric + "&type=" + type + '&whereFieldForType='+search + '&timePeriod='+period+'&aggregationInterval='+aggregation,
                type: "GET",
                success: function (data) {
                    var ctx = document.getElementById(tag).getContext("2d");
                    var timechart = new Chart(ctx, config);
                    addData(timechart, data.series[0].data, data.series[0].name)
                },
                error: function (request, status, error) {
                    // alert("Problem getting data from server " + request.responseText + " " + error);
                    console.log("Problem getting data from server " + request.responseText + " " + error)
                }
            });
        }

        function addData(chart, data, label) {
            var newDataset = {
                label: label,
                data: data,
                backgroundColor: color,
                borderColor: color,
            };
            chart.data.datasets.push(newDataset);
            chart.update();
        }

        callAjax(tag, config, metric, type);

    },
};